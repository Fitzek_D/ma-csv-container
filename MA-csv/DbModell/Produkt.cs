﻿using System;
using System.Collections.Generic;

namespace MA_csv.DbModell
{
    public class Produkt
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }

        public ICollection<KundeProdukt> Kunden { get; set; }
    }

}
