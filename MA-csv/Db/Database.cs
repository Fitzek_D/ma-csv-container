﻿using System;
using MA_csv.DbModell;
using Microsoft.EntityFrameworkCore;

namespace MA_csv.Db
{
    public class Database : DbContext
    {
        public Database(DbContextOptions<Database> options)
            : base(options)
        {
        }

        public DbSet<ZvooveBenutzer> ZvooveBenutzer { get; set; }
        public DbSet<Benutzer> Benutzer { get; set; }
        public DbSet<Kunde> Kunden { get; set; }
        public DbSet<Produkt> Produkte { get; set; }
        public DbSet<Auftrag> Auftraege { get; set; }
        public DbSet<Zeitarbeiter> Zeitarbeiter { get; set; }
        public DbSet<Lohn> Loehne { get; set; }
        public DbSet<ZeitarbeiterAuftrag> ZeitarbeiterAuftraege { get; set; }
        public DbSet<KundeProdukt> KundenProdukte { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ZvooveBenutzer>().ToTable("ZvooveBenutzer");
            modelBuilder.Entity<Benutzer>().ToTable("Benutzer");
            modelBuilder.Entity<Kunde>().ToTable("Kunde");
            modelBuilder.Entity<Produkt>().ToTable("Produkt");
            modelBuilder.Entity<Auftrag>().ToTable("Auftrag");
            modelBuilder.Entity<Zeitarbeiter>().ToTable("Zeitarbeiter");
            modelBuilder.Entity<Lohn>().ToTable("Lohn");
            modelBuilder.Entity<ZeitarbeiterAuftrag>().ToTable("ZeitarbeiterAuftrag");
            modelBuilder.Entity<KundeProdukt>().ToTable("KundeProdukt");
        }
    }

}
