﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using MA_csv.Db;
using MA_csv.DbModell;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MA_csv.Controllers
{
    [Route("api/[controller]/[action]")]
    public class CsvController : Controller
    {
        [HttpGet]
        [ActionName("get-lohnabrechnung")]
        public string GetLohnabrechnung([FromServices] Database db, int id)
        {
            var zeitarbeiter = db.Zeitarbeiter.SingleOrDefault(a => a.Id == id);

            var loehne = db.Loehne.Where(l => l.Zeitarbeiter.Id == zeitarbeiter.Id).ToList();

            string result;

            var data = new Lohn();
            using (var mem = new MemoryStream())
            using (var writer = new StreamWriter(mem))
            using (var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                csvWriter.WriteHeader<Lohn>();
                csvWriter.NextRecord();
                foreach (var lohn in loehne)
                {
                    csvWriter.WriteRecord(lohn);
                    csvWriter.NextRecord();
                }

                writer.Flush();
                result = Encoding.UTF8.GetString(mem.ToArray());
                Console.WriteLine(result);
            }

            return result;
        }

        [HttpGet]
        [ActionName("get-auftraege")]
        public string GetAuftraege([FromServices] Database db, int id)
        {
            var kunde = db.Kunden.SingleOrDefault(a => a.Id == id);

            var auftraege = db.Auftraege.Where(l => l.Kunde.Id == kunde.Id).ToList();

            string result;

            var data = new Lohn();
            using (var mem = new MemoryStream())
            using (var writer = new StreamWriter(mem))
            using (var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                csvWriter.WriteHeader<Auftrag>();
                csvWriter.NextRecord();
                foreach (var auftrag in auftraege)
                {
                    csvWriter.WriteRecord(auftrag);

                    csvWriter.NextRecord();
                }

                writer.Flush();
                result = Encoding.UTF8.GetString(mem.ToArray());
                Console.WriteLine(result);
            }

            return result;
        }
    }
}
