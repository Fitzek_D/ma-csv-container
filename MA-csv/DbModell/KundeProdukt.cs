﻿using System;
namespace MA_csv.DbModell
{
    public class KundeProdukt
    {
        public int Id { get; set; }

        public Kunde Kunde { get; set; }
        public Produkt Produkt { get; set; }
    }


}
