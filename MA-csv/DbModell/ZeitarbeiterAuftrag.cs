﻿using System;
namespace MA_csv.DbModell
{
    public class ZeitarbeiterAuftrag
    {
        public int Id { get; set; }

        public Zeitarbeiter Zeitarbeiter { get; set; }
        public Auftrag Auftrag { get; set; }
    }
}
